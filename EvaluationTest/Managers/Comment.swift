//
//  Comment.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import Foundation

class Comment {
    
    var id: Int?
    var email: String?
    var body: String?
    var name: String?
    var postId : Int?
    
    init(id: Int?, email: String?, body: String?, name: String?, postId : Int?) {
        self.id = id
        self.email = email
        self.body = body
        self.name = name
        self.postId = postId
    }
}
