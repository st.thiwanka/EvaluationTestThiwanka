//
//  Post.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import Foundation

class Post {
    
    var id: Int?
    var title: String?
    var body: String?
    var userId: Int?
    
    init(id: Int?, title: String?, body: String?, userId: Int?) {
        self.id = id
        self.title = title
        self.body = body
        self.userId = userId
    }
}
