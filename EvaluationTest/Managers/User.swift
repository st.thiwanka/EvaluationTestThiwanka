//
//  User.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import Foundation

class User {
    
    var id: Int?
    var name: String?
    var phone: String?
    var email: String?
    var city : String?
    var street : String?
    var suite : String?
    var website : String?
    var zipcode : String?
    var lat : String?
    var lng : String?
    
    
    init(id: Int?, name: String?, phone: String?, email: String?, city : String?, street : String?, suite : String?, website : String?, zipcode : String?, lat : String?, lng : String?) {
        self.id = id
        self.name = name
        self.phone = phone
        self.email = email
        self.city = city
        self.street = street
        self.suite = suite
        self.website = website
        self.zipcode = zipcode
        self.lat = lat
        self.lng = lng
    }
}
