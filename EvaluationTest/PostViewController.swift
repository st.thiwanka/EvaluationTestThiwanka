//
//  PostViewController.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import AlamofireImage
import SwiftyJSON

class PostViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var Post: Post?
    var user : User?
    
    @IBOutlet weak var PostTitle: UILabel!
    @IBOutlet weak var PostDescription: UILabel!
    @IBOutlet weak var UserNameLabel: UILabel!
    @IBOutlet weak var AvatarHolder: UIImageView!
    @IBOutlet weak var CommentsCount: UILabel!
    @IBOutlet weak var CommentsList: UITableView!
    
    var Comments = [Comment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.PostTitle.text = self.Post?.title
        self.PostDescription.text = self.Post?.body
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let requestComments = NSFetchRequest<NSFetchRequestResult>(entityName: "Comments")
        
        let postId = Post?.id!
        let predicateComments = NSPredicate(format: "postId == %@", String(postId!))
        requestComments.predicate = predicateComments
        
        do {
            let result = try managedContext.fetch(requestComments)
            self.CommentsCount.text = String(result.count) + " Comments"
            
            for comment in result {
                let id = (comment as AnyObject).value(forKey: "id") as! Int
                let email = (comment as AnyObject).value(forKey: "email") as! String
                let body = (comment as AnyObject).value(forKey: "body") as! String
                let name = (comment as AnyObject).value(forKey: "name") as! String
                let postId = (comment as AnyObject).value(forKey: "postId") as! Int
                
                self.Comments.append(Comment(
                    id: id,
                    email: email,
                    body: body,
                    name: name,
                    postId : postId
                ))
            }
            
            self.CommentsList.reloadData()
        } catch {
            
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        let UserID = Post?.userId!
        let predicate = NSPredicate(format: "id == %@", String(UserID!))
        request.predicate = predicate
        request.fetchLimit = 1
        
        do {
            let result = try managedContext.fetch(request)
            if result.count == 1 {
                let data = result.first!
                let name  = ((data as AnyObject).value(forKey: "name") as? String)!
                self.UserNameLabel.text = name
                
                let email  = ((data as AnyObject).value(forKey: "email") as? String)!
                let phone  = ((data as AnyObject).value(forKey: "phone") as? String)!
                let city  = ((data as AnyObject).value(forKey: "city") as? String)!
                let street  = ((data as AnyObject).value(forKey: "street") as? String)!
                let suite  = ((data as AnyObject).value(forKey: "suite") as? String)!
                let website  = ((data as AnyObject).value(forKey: "website") as? String)!
                let zipcode  = ((data as AnyObject).value(forKey: "zipcode") as? String)!
                let lat  = ((data as AnyObject).value(forKey: "lat") as? String)!
                let lng  = ((data as AnyObject).value(forKey: "lng") as? String)!
                
                self.user = User(
                    id: 1,
                    name : name,
                    phone : phone,
                    email : email,
                    city : city,
                    street : street,
                    suite : suite,
                    website : website,
                    zipcode : zipcode,
                    lat : lat,
                    lng : lng
                )
                
                let imageURL = "https://api.adorable.io/avatars/64/" + email
                AvatarHolder.contentMode = UIViewContentMode.scaleToFill;
                AvatarHolder.image = UIImage(named: "user_image")
                Alamofire.request(imageURL).responseImage { response in
                    if let image = response.result.value {
                        self.AvatarHolder.image = image
                    }
                }
            }
        } catch {
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if segue.identifier == "UserDetail"{
            if let UserViewController = destination as? UserViewController {
                UserViewController.User = self.user
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentTableViewCell
        
        let comment: Comment
        comment = self.Comments[indexPath.row]
        
        cell.CommentLabel.text = comment.name
        
        return cell
    }
}
