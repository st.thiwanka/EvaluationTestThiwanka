//
//  CommentTableViewCell.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var CommentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
