//
//  UserViewController.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MapKit

class UserViewController: UIViewController {

    var User : User?
    
    @IBOutlet weak var AvatarHolder: UIImageView!
    @IBOutlet weak var UserEmailLabel: UILabel!
    @IBOutlet weak var UserPhoneLabel: UILabel!
    @IBOutlet weak var UserNameLabel: UILabel!
    @IBOutlet weak var StreetLabel: UILabel!
    @IBOutlet weak var SuiteLabel: UILabel!
    @IBOutlet weak var CityLabel: UILabel!
    @IBOutlet weak var ZipLabel: UILabel!
    @IBOutlet weak var WebUrlLabel: UILabel!
    @IBOutlet weak var MapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.UserNameLabel.text = User?.name!
        self.UserEmailLabel.text = User?.email!
        self.UserPhoneLabel.text = User?.phone!
        self.StreetLabel.text = User?.street!
        self.SuiteLabel.text = User?.suite!
        self.CityLabel.text = User?.city!
        self.ZipLabel.text = User?.zipcode!
        self.WebUrlLabel.text = User?.website!
        
        let imageURL = "https://api.adorable.io/avatars/64/" + (User?.email!)!
        AvatarHolder.contentMode = UIViewContentMode.scaleToFill;
        AvatarHolder.image = UIImage(named: "user_image")
        Alamofire.request(imageURL).responseImage { response in
            if let image = response.result.value {
                self.AvatarHolder.image = image
            }
        }
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(16, 16)
        
        let lattitude = User?.lat!
        let lattitudeDouble = Double(lattitude!)
        
        let longitude = User?.lng!
        let longitudeDouble = Double(longitude!)
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lattitudeDouble!, longitudeDouble!)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        MapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = location
        MapView.addAnnotation(annotation)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
