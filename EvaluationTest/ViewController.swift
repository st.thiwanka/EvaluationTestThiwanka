//
//  ViewController.swift
//  EvaluationTest
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let evaluation = Evaluation()
    var Posts = [Post]()
    
    @IBOutlet weak var PostsList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if evaluation.connectedToNetwork() {
            self.SavePosts()
            self.SaveUsers()
            self.SaveComments()
        }
        
        self.getPosts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPosts(){
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Posts")
        
        do {
            let result = try managedContext.fetch(request)
            for post in result {
                
                let id = (post as AnyObject).value(forKey: "id") as! Int
                let title = (post as AnyObject).value(forKey: "title") as! String
                let body = (post as AnyObject).value(forKey: "body") as! String
                let userId = (post as AnyObject).value(forKey: "userId") as! Int
                
                self.Posts.append(Post(
                    id: id,
                    title: title,
                    body: body,
                    userId: userId
                ))
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        self.PostsList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as! PostTableViewCell
        
        let post: Post
        post = self.Posts[indexPath.row]
        
        cell.PostTitle.text = post.title
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if segue.identifier == "PostDetail"{
            if let indexPath = self.PostsList.indexPathForSelectedRow{
                if let PostViewController = destination as? PostViewController {
                    
                    let post: Post
                    post = self.Posts[indexPath.row]
                    
                    PostViewController.Post = post
                }
            }
        }
    }
    
    func SavePosts(){
        let API_URL = "http://jsonplaceholder.typicode.com/posts"
        Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
            if let jsonValue = responseData.result.value {
                let json = JSON(jsonValue)
                let PostArray : NSArray  = json.array! as NSArray
                
                guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                }
                
                for i in 0..<PostArray.count{
                    let post = JSON(PostArray[i])
                    
                    self.Posts.append(Post(
                        id: post["id"].int,
                        title: post["title"].string,
                        body: post["body"].string,
                        userId: post["userId"].int
                    ))
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Posts", in: managedContext)!
                    let SavePost = NSManagedObject(entity: entity, insertInto: managedContext)
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Posts")
                    
                    let PostID = post["id"].int!
                    let predicate = NSPredicate(format: "id == %@", String(PostID))
                    request.predicate = predicate
                    request.fetchLimit = 1
                    
                    do{
                        let count = try managedContext.count(for: request)
                        if(count == 0){
                            // no matching object
                            SavePost.setValue(post["id"].int, forKeyPath: "id")
                            SavePost.setValue(post["userId"].int, forKeyPath: "userId")
                            SavePost.setValue(post["title"].string, forKeyPath: "title")
                            SavePost.setValue(post["body"].string, forKeyPath: "body")
                            
                            do {
                                try managedContext.save()
                            }
                            catch let error as NSError {
                                print("Could not save \(error), \(error.userInfo)")
                            }
                        }
                        else{
                            // at least one matching object exists
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                    }
                }
            }
            
            self.PostsList.reloadData()
        }
    }
    
    func SaveUsers(){
        let API_URL = "http://jsonplaceholder.typicode.com/users"
        Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
            if let jsonValue = responseData.result.value {
                let json = JSON(jsonValue)
                let UsersArray : NSArray  = json.array! as NSArray
                
                guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                }
                
                for i in 0..<UsersArray.count{
                    let User = JSON(UsersArray[i])
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
                    let SaveUser = NSManagedObject(entity: entity, insertInto: managedContext)
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
                    
                    let UserID = User["id"].int!
                    let predicate = NSPredicate(format: "id == %@", String(UserID))
                    request.predicate = predicate
                    request.fetchLimit = 1
                    
                    do{
                        let count = try managedContext.count(for: request)
                        if(count == 0){
                            // no matching object
                            SaveUser.setValue(User["id"].int, forKeyPath: "id")
                            SaveUser.setValue(User["email"].string, forKeyPath: "email")
                            SaveUser.setValue(User["name"].string, forKeyPath: "name")
                            SaveUser.setValue(User["username"].string, forKeyPath: "username")
                            SaveUser.setValue(User["phone"].string, forKeyPath: "phone")
                            SaveUser.setValue(User["website"].string, forKeyPath: "website")
                            
                            let Address = JSON(User["address"])
                            let Geo = JSON(Address["geo"])
                            
                            SaveUser.setValue(Address["street"].string, forKeyPath: "street")
                            SaveUser.setValue(Address["suite"].string, forKeyPath: "suite")
                            SaveUser.setValue(Address["city"].string, forKeyPath: "city")
                            SaveUser.setValue(Address["zipcode"].string, forKeyPath: "zipcode")
                            
                            SaveUser.setValue(Geo["lat"].string, forKeyPath: "lat")
                            SaveUser.setValue(Geo["lng"].string, forKeyPath: "lng")
                            
                            do {
                                try managedContext.save()
                            }
                            catch let error as NSError {
                                print("Could not save \(error), \(error.userInfo)")
                            }
                        }
                        else{
                            // at least one matching object exists
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                    }
                }
            }
        }
    }
    
    func SaveComments(){
        let API_URL = "http://jsonplaceholder.typicode.com/comments"
        Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
            if let jsonValue = responseData.result.value {
                let json = JSON(jsonValue)
                let CommentsArray : NSArray  = json.array! as NSArray
                
                guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                }
                
                for i in 0..<CommentsArray.count{
                    let Comment = JSON(CommentsArray[i])
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Comments", in: managedContext)!
                    let SaveComment = NSManagedObject(entity: entity, insertInto: managedContext)
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Comments")
                    
                    let CommentID = Comment["id"].int!
                    let predicate = NSPredicate(format: "id == %@", String(CommentID))
                    request.predicate = predicate
                    request.fetchLimit = 1
                    
                    do{
                        let count = try managedContext.count(for: request)
                        if(count == 0){
                            // no matching object
                            SaveComment.setValue(Comment["id"].int, forKeyPath: "id")
                            SaveComment.setValue(Comment["postId"].int, forKeyPath: "postId")
                            SaveComment.setValue(Comment["name"].string, forKeyPath: "name")
                            SaveComment.setValue(Comment["email"].string, forKeyPath: "email")
                            SaveComment.setValue(Comment["body"].string, forKeyPath: "body")
                            
                            do {
                                try managedContext.save()
                            }
                            catch let error as NSError {
                                print("Could not save \(error), \(error.userInfo)")
                            }
                        }
                        else{
                            // at least one matching object exists
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                    }
                }
            }
        }
    }
}

