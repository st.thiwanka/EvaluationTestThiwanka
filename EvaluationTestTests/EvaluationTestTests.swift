//
//  EvaluationTestTests.swift
//  EvaluationTestTests
//
//  Created by Thiwanka Nawarathne on 5/17/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import XCTest
import Alamofire
import AlamofireImage
import SwiftyJSON
import CoreData

@testable import EvaluationTest

class EvaluationTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetPosts() {
        let API_URL = "http://jsonplaceholder.typicode.com/posts"
        Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
            if let jsonValue = responseData.result.value {
                let json = JSON(jsonValue)
                let PostArray : NSArray  = json.array! as NSArray
                print(PostArray.count)
                
                guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                }
                
                for i in 0..<PostArray.count{
                    let post = JSON(PostArray[i])
                    
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Posts", in: managedContext)!
                    let SavePost = NSManagedObject(entity: entity, insertInto: managedContext)
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Posts")
                    
                    let PostID = post["id"].int!
                    let predicate = NSPredicate(format: "id == %@", String(PostID))
                    request.predicate = predicate
                    request.fetchLimit = 1
                    
                    do{
                        let count = try managedContext.count(for: request)
                        if(count == 0){
                            // no matching object
                            SavePost.setValue(post["id"].int, forKeyPath: "id")
                            SavePost.setValue(post["userId"].int, forKeyPath: "userId")
                            SavePost.setValue(post["title"].string, forKeyPath: "title")
                            SavePost.setValue(post["body"].string, forKeyPath: "body")
                            
                            do {
                                try managedContext.save()
                            }
                            catch let error as NSError {
                                print("Could not save \(error), \(error.userInfo)")
                            }
                        }
                        else{
                            // at least one matching object exists
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                    }
                }
            }
        }
    }
    
    func testGetUsers() {
        let API_URL = "http://jsonplaceholder.typicode.com/users"
        Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
            if let jsonValue = responseData.result.value {
                let json = JSON(jsonValue)
                let UsersArray : NSArray  = json.array! as NSArray
                print(UsersArray.count)
                
                guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                }
                
                for i in 0..<UsersArray.count{
                    let User = JSON(UsersArray[i])
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
                    let SaveUser = NSManagedObject(entity: entity, insertInto: managedContext)
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
                    
                    let UserID = User["id"].int!
                    let predicate = NSPredicate(format: "id == %@", String(UserID))
                    request.predicate = predicate
                    request.fetchLimit = 1
                    
                    do{
                        let count = try managedContext.count(for: request)
                        if(count == 0){
                            // no matching object
                            SaveUser.setValue(User["id"].int, forKeyPath: "id")
                            SaveUser.setValue(User["email"].string, forKeyPath: "email")
                            SaveUser.setValue(User["name"].string, forKeyPath: "name")
                            SaveUser.setValue(User["username"].string, forKeyPath: "username")
                            SaveUser.setValue(User["phone"].string, forKeyPath: "phone")
                            SaveUser.setValue(User["website"].string, forKeyPath: "website")
                            
                            let Address = JSON(User["address"])
                            let Geo = JSON(Address["geo"])
                            
                            SaveUser.setValue(Address["street"].string, forKeyPath: "street")
                            SaveUser.setValue(Address["suite"].string, forKeyPath: "suite")
                            SaveUser.setValue(Address["city"].string, forKeyPath: "city")
                            SaveUser.setValue(Address["zipcode"].string, forKeyPath: "zipcode")
                            
                            SaveUser.setValue(Geo["lat"].string, forKeyPath: "lat")
                            SaveUser.setValue(Geo["lng"].string, forKeyPath: "lng")
                            
                            do {
                                try managedContext.save()
                            }
                            catch let error as NSError {
                                print("Could not save \(error), \(error.userInfo)")
                            }
                        }
                        else{
                            // at least one matching object exists
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                    }
                }
            }
        }
    }
    
    func testGetComments() {
        let API_URL = "http://jsonplaceholder.typicode.com/comments"
        Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
            if let jsonValue = responseData.result.value {
                let json = JSON(jsonValue)
                let CommentsArray : NSArray  = json.array! as NSArray
                print(CommentsArray.count)
                
                guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                }
                
                for i in 0..<CommentsArray.count{
                    let Comment = JSON(CommentsArray[i])
                    
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Comments", in: managedContext)!
                    let SaveComment = NSManagedObject(entity: entity, insertInto: managedContext)
                    
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Comments")
                    
                    let CommentID = Comment["id"].int!
                    let predicate = NSPredicate(format: "id == %@", String(CommentID))
                    request.predicate = predicate
                    request.fetchLimit = 1
                    
                    do{
                        let count = try managedContext.count(for: request)
                        if(count == 0){
                            // no matching object
                            SaveComment.setValue(Comment["id"].int, forKeyPath: "id")
                            SaveComment.setValue(Comment["postId"].int, forKeyPath: "postId")
                            SaveComment.setValue(Comment["name"].string, forKeyPath: "name")
                            SaveComment.setValue(Comment["email"].string, forKeyPath: "email")
                            SaveComment.setValue(Comment["body"].string, forKeyPath: "body")
                            
                            do {
                                try managedContext.save()
                            }
                            catch let error as NSError {
                                print("Could not save \(error), \(error.userInfo)")
                            }
                        }
                        else{
                            // at least one matching object exists
                        }
                    }
                    catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                    }
                }
            }
        }
    }
    
    func testGetAvatar(){
        let imageURL = "https://api.adorable.io/avatars/64/st.thiwanka@live.com"
        Alamofire.request(imageURL).responseImage { response in
            print("image")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
